from fastapi import FastAPI
from dfcreator import dummydfcreator, oms_dfdata

app = FastAPI()

@app.get("/df/")
async def jsonizador():
    json_string = dummydfcreator()
    return {"df": json_string}

@app.get("/oms/")
async def omsjson():
    json_string = oms_dfdata()
    return {"oms": json_string}


