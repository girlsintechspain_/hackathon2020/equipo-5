# streamlit-fastapi-model-serving

World map visualization for variable with value within three categories.

Uses streamlit and FastAPI for ML model serving described on [this blogpost](https://davidefiocco.github.io/2020/06/27/streamlit-fastapi-ml-serving.html) and [PyConES 2020 video](https://www.youtube.com/watch?v=IvHCxycjeR0).

The target is to serve the outcome of a category model using `FastAPI` for the backend service and `streamlit` for the frontend service. `docker-compose` orchestrates the two services and allows communication between them.

To run the example in a machine running Docker and docker-compose, run:

    docker-compose build
    docker-compose up

To visit the FastAPI documentation of the resulting service, visit http://localhost:8000 with a web browser.  
To visit the streamlit UI, visit http://localhost:8501.

Logs can be inspected via:

    docker-compose logs